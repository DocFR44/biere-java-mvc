<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<h2>Ajouter un commentaire</h2>

<div id="form-rech-bieres">

<form method="get" action="${pageContext.request.contextPath}/membre/ajout-commentaire" enctype="multipart/form-data">
	
	<p>${requestScope.modAjoutCommentaire.msgErreur}</p>
	<p>${requestScope.CritiqueAjoute}</p>	
	<p>
		<label for="Arome">Arôme: </label>		
		<c:choose>
			<c:when test="${sessionScope.AromeAjout != null}">
				<input name="Arome" id="Arome" type="number" min="0" step="1" max = "10" value="${sessionScope.AromeAjout}"/>
			</c:when>
			<c:otherwise>
				<input name="Arome" id="Arome" type="number" min="0" step="1" max = "10" />
			</c:otherwise>	
		</c:choose>	
	</p>
	
	<p>
		<label for="arome">Arôme: </label>		
		<c:choose>
			<c:when test="${sessionScope.AromeAjout != null}">
				<input name="arome" id="arome" type="number" min="0" step="1" max = "10" value="${sessionScope.AromeAjout}"/>
			</c:when>
			<c:otherwise>
				<input name="arome" id="arome" type="number" min="0" step="1" max = "10" />
			</c:otherwise>	
		</c:choose>	
	</p>
	
	<p>
		<label for="apparence">Apparence: </label>		
		<c:choose>
			<c:when test="${sessionScope.ApparenceAjout != null}">
				<input name="apparence" id="apparence" type="number" min="0" step="1" max = "10" value="${sessionScope.ApparenceAjout}"/>
			</c:when>
			<c:otherwise>
				<input name="apparence" id="apparence" type="number" min="0" step="1" max = "10" />
			</c:otherwise>	
		</c:choose>	
	</p>
	
	<p>
		<label for="gout">Goût: </label>		
		<c:choose>
			<c:when test="${sessionScope.GoutAjout != null}">
				<input name="gout" id="gout" type="number" min="0" step="1" max = "10" value="${sessionScope.GoutAjout}"/>
			</c:when>
			<c:otherwise>
				<input name="gout" id="gout" type="number" min="0" step="1" max = "10" />
			</c:otherwise>	
		</c:choose>	
	</p>
	
	<p>
		<label for="style">Style de la bouteille: </label>		
		<c:choose>
			<c:when test="${sessionScope.StyleAjout != null}">
				<input name="style" id="style" type="number" min="0" step="1" max = "10" value="${sessionScope.StyleAjout}"/>
			</c:when>
			<c:otherwise>
				<input name="style" id="style" type="number" min="0" step="1" max = "10" />
			</c:otherwise>	
		</c:choose>	
	</p>
	
	<p>
		<label for="general">Général: </label>		
		<c:choose>
			<c:when test="${sessionScope.GeneralAjout != null}">
				<input name="general" id="general" type="number" min="0" step="1" max = "10" value="${sessionScope.GeneralAjout}"/>
			</c:when>
			<c:otherwise>
				<input name="general" id="general" type="number" min="0" step="1" max = "10" />
			</c:otherwise>	
		</c:choose>	
	</p>
		
		<input type="hidden" name= "noBiere" value="${param['noBiere']}" />
	
	<p>
		<input type="submit" name="ajout-critique" value="Ajouter un commentaire" class="btn" />
	</p>
</form>
</div>