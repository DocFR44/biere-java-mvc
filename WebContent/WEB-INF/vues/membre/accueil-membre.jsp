<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<%-- Affichage des informations du membre du personnel --%>
<h2>Vos informations</h2>
<h3>${(sessionScope.util.nom)}</h3>
<h4>Vos 10 derniers commentaires</h4>
<table>
	<tr>
		<th>Date de création</th>
		<th>Nom de la bière</th>
		<th>Arôme</th>
		<th>Apparence</th>
		<th>Goût</th>
		<th>Style bouteille</th>
		<th>Général</th>
	</tr>
	<%-- Parcours et affichage des bières trouvées --%>
	<c:forEach var="critique" items="${requestScope.modAccueilMembre.lstCritiqueTrouvees}">
		<tr>
			<tr>
			<td>${critique.date_creation}</td>
			<td>${critique.nomBiere}</td>
			<td>${critique.arome}</td>
			<td>${critique.apparence}</td>
			<td>${critique.gout}</td>
			<td>${critique.style_bouteille}</td>
			<td>${critique.general}</td>
		</tr>
	</c:forEach>
</table>