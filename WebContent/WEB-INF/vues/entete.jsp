<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!-- 
<img src="${pageContext.request.contextPath}/images/logo-biere.png" alt="Logo Les bières du monde" />
-->
<div id="section-connexion">

	<c:choose>							
		<c:when test="${(sessionScope.util == null)}">
			
		
	<%-- Formulaire de connexion --%>
	<form method="post" action="${pageContext.request.contextPath}/connexion">
		<p>
			<label for="identifiant">Identifiant : </label>
			<c:choose>							
				<c:when test="${(requestScope.username == null)}">
					<input type="text" name="identifiant" id="identifiant" class="txt" />
				</c:when>
				<c:otherwise>
					<input type="text" name="identifiant" id="identifiant" class="txt" value="${(requestScope.username)}" />
				</c:otherwise>	
			</c:choose>	
		</p>
		<p>
			<label for="motPasse">Mot de passe : </label>
			<input type="password" name="motPasse" id="motPasse" class="txt" onfocus="this.value=''" />
		</p>
		<p>
			<label for="typeConn">Personnel : </label>
				<c:choose>							
					<c:when test="${(requestScope.isChecked == null)}">
						<input type="checkbox" name="typeConn" id="typeConn" value="admin" />
					</c:when>
					<c:otherwise>
						<input type="checkbox" name="typeConn" id="typeConn" value="admin" checked/>
					</c:otherwise>	
			</c:choose>	
			<input type="submit" name="btnConnexion" value="Connexion" class="btn" />
		</p>
	</form>
	</c:when>
		<c:otherwise>
		<c:choose>							
			<c:when test="${(sessionScope.util.modeConn == 'ADMIN')}">		
				<p>(${sessionScope.util.nomUtil})</p>
				<p>Employé</p>
			</c:when>
			<c:otherwise>
				<p>${sessionScope.util.nom}</p>
			</c:otherwise>	
		</c:choose>	
			<form method="get" action="${pageContext.request.contextPath}/deconnexion">
					<input type="submit" name="btnDeconnexion" value="Deconnexion" class="btn" />
			</form>
		</c:otherwise>	
	</c:choose>	
	<p>${requestScope.msgErreur}</p>
</div>

<h1>Les bières du monde</h1>
