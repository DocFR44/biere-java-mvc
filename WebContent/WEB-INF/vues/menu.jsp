<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<ul id="menu-princ">
	<c:if test="${(sessionScope.util.modeConn == 'ADMIN')}">
		<li><a href="${pageContext.request.contextPath}/admin">Vos informations</a></li>
	</c:if>
	<c:if test="${(sessionScope.util.modeConn == 'MEMBRE')}">
		<li><a href="${pageContext.request.contextPath}/membre">Vos informations</a></li>
	</c:if>
	<li><a href="${pageContext.request.contextPath}/rech-bieres">Recherche de bières</a></li>
	<li><a href="${pageContext.request.contextPath}/brasseurs">Les brasseurs</a></li>

</ul>  <!-- Fin "menu-princ" -->
