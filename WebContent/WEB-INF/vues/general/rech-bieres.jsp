<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

	<c:if test="${(requestScope.messageConfirmation != null)}">
		<p>${(requestScope.messageConfirmation)}</p>
	</c:if>
<h2>Recherche de bières</h2>

<!-- Formulaire de recherche de bières -->
<div id="form-rech-bieres">
	<form method="get" action="${pageContext.request.contextPath}/rech-bieres">
			

			<p>${requestScope.modRechBieres.msgErreur}</p>


		<p>
			<%-- Champ texte pour le mot clé; initialisé avec la valeur du paramètre
				 pour les critères de recherche --%>
			<label for="mot-cle">Mot clé : </label>
			<c:choose>
				<c:when test="${sessionScope.motCleRech != null}">
					<input type="text" name="mot-cle" id="mot-cle" value="${sessionScope.motCleRech}" />
				</c:when>
				<c:otherwise>
					<input type="text" name="mot-cle" id="mot-cle" value="${fn:trim(param['mot-cle'])}" />
				</c:otherwise>
			</c:choose>
			
		</p>
		<p>


			<label for="categorie">Catégorie : </label>

			<select name="categorie" id="categorie">

				<option>Toutes</option>
								
				<c:forEach var="categorie" items="${requestScope.modListCat.lstCategorie}">
					<c:choose>
						<c:when test="${sessionScope.catRech != null}">
							<c:choose>							
								<c:when test="${(sessionScope.catRech == categorie.id)}">
									<option value="${categorie.id}" selected="selected">${categorie.nom}</option>
								</c:when>
								<c:otherwise>
									<option value="${categorie.id}">${categorie.nom}</option>
								</c:otherwise>	
							</c:choose>			
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${(param['categorie'] == categorie.id)}">
									<option value="${categorie.id}" selected="selected">${categorie.nom}</option>
								</c:when>
								<c:otherwise>
									<option value="${categorie.id}">${categorie.nom}</option>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
		</p>
		<p>	
			<label for="tauxAlcoolMin">Taux d'alcool minimum :</label>
			<c:choose>
				<c:when test="${sessionScope.motCleRech != null}">
					<input name="tauxAlcoolMin" id="tauxAlcoolMin" type="number" min="0" step="0.5" value="${sessionScope.alcMinRech}"/>
				</c:when>
				<c:otherwise>
					<input type="text" name="tauxAlcoolMin" id="tauxAlcoolMin" value="${fn:trim(param['tauxAlcoolMin'])}" />
				</c:otherwise>
			</c:choose>
			
		</p>
		<p>
			<label for="tauxAlcoolMax">Taux d'alcool maximum :</label>
			<c:choose>
				<c:when test="${sessionScope.motCleRech != null}">
					<input name="tauxAlcoolMax" id="tauxAlcoolMax" type="number" min="0" step="0.5" value="${sessionScope.alcMaxRech}"/>
				</c:when>
				<c:otherwise>
					<input name="tauxAlcoolMax" id="tauxAlcoolMax" type="number" min="0" step="0.5" value="${param['tauxAlcoolMax']}"/>
				</c:otherwise>
			</c:choose>
			
		</p>
		
		<p>
			<input type="submit" name="rech-bieres" value="Rechercher les bieres" class="btn" />
		</p>
	</form>
</div>  <!-- Fin de la division "formRechBieres" -->


<c:if test="${not empty param['rech-bieres']}">

	<h2>Bières trouvées (${requestScope.modRechBieres.nbrBiere})</h2>

	<c:choose>

		<c:when test="${empty requestScope.modRechBieres.lstBieresTrouvees}">
			<p>Aucune bière trouvée</p>
		</c:when>
			
		<c:otherwise>

			<table>
				<tr>
					<th>Nom</th>
					<th>Brasseur</th>
					<th>Catégorie</th>
					<th>Taux d'alcool</th>
				</tr>
				<%-- Parcours et affichage des bières trouvées --%>
				<c:forEach var="biere" items="${requestScope.modRechBieres.lstBieresTrouvees}">
					<tr>
						<tr>
						<td><a href="${pageContext.request.contextPath}/details-biere?noBiere=${biere.noBiere}">${biere.nom}</a></td>
						<td>${biere.nomBrasseur}</td>
						<td>${biere.nomCategorie}</td>
						<td>${biere.tauxAlcool} %</td>
					</tr>
				</c:forEach>
			</table>
			
			<!-- Barre de navigation pour les pages de résultats de la recherche de bières -->
			<div id="nav-page-bieres">
			
	
				<c:if test="${(param['page'] != null) && (param['page'] > 1)}">
					<a href="${request.getRequestURL()}?mot-cle=${(param['mot-cle'])}&amp;categorie=${(param['categorie'])}&amp;tauxAlcoolMin=${(param['tauxAlcoolMin'])}&amp;tauxAlcoolMax=${(param['tauxAlcoolMax'])}&amp;rech-bieres=${(param['rech-bieres'])}&amp;page=${param['page']-1}"">
						<img src="${pageContext.request.contextPath}/images/fleche-gauche.png" alt="Page précédente"/>
						Page précédente
					</a>
				</c:if>

				|
				<c:if test="${(param['page'] < (requestScope.modRechBieres.nbrBiere / 10)) || (param['page'] == null && 1 < (requestScope.modRechBieres.nbrBiere / 10))}">
				<c:choose>
					<c:when test="${(param['page'] == null)}">
						<a href="${request.getRequestURL()}?mot-cle=${(param['mot-cle'])}&amp;categorie=${(param['categorie'])}&amp;tauxAlcoolMin=${(param['tauxAlcoolMin'])}&amp;tauxAlcoolMax=${(param['tauxAlcoolMax'])}&amp;rech-bieres=${(param['rech-bieres'])}&amp;page=2">
							Page suivante
							<img src="${pageContext.request.contextPath}/images/fleche-droite.png" alt="Page suivante"/>
						</a>
					</c:when>
					<c:otherwise>
					
						<a href="${request.getRequestURL()}?mot-cle=${(param['mot-cle'])}&amp;categorie=${(param['categorie'])}&amp;tauxAlcoolMin=${(param['tauxAlcoolMin'])}&amp;tauxAlcoolMax=${(param['tauxAlcoolMax'])}&amp;rech-bieres=${(param['rech-bieres'])}&amp;page=${param['page']+1}">
							Page suivante
							<img src="${pageContext.request.contextPath}/images/fleche-droite.png" alt="Page suivante"/>
						</a>
					</c:otherwise>
				</c:choose>
				</c:if>
				
				
			</div>

		</c:otherwise>

	</c:choose>
			
</c:if>
