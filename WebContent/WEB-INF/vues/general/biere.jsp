<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<h2>Détails de la bière no. ${param['noBiere']}</h2>

<h3>${requestScope.modBiereDetail.uneBiere.nom}</h3>
<p>Nom du brasseur : ${requestScope.modBiereDetail.uneBiere.nomBrasseur}</p>
<p>Catégorie : ${requestScope.modBiereDetail.uneBiere.nomCategorie}</p>
<p>Taux d'alcool : ${requestScope.modBiereDetail.uneBiere.tauxAlcool} %</p>
<p>Description : ${requestScope.modBiereDetail.uneBiere.description}</p>
<c:choose>
	<c:when test= "${empty requestScope.modBiereDetail.uneBiere.image}">
		<img src="${pageContext.request.contextPath}/images/bieres/base.png" />
	</c:when>
	<c:otherwise>
		<img src="${pageContext.request.contextPath}/images/bieres/${requestScope.modBiereDetail.uneBiere.image}" />
	</c:otherwise>
</c:choose>

<c:if test="${(sessionScope.util.modeConn == 'MEMBRE')}">
<form method="get" action="${pageContext.request.contextPath}/membre/ajout-commentaire" enctype="multipart/form-data">
	<input type="hidden" name= "noBiere" value="${param['noBiere']}" />
	<input type="submit" name="critique" value="Ajouter une critique" class="btn" />
</form>
</c:if>