<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h2>Les brasseurs</h2>

<%-- Parcours et affichage des brasseurs --%>
<c:forEach var="brass" items="${requestScope.modBrasseur.lstBrasseurs}">
	<div class="brasseur">
		<h3>Brasseur no. ${brass.id}</h3>
		<div class="details-brasseur">	
			<c:if test="${not empty brass.img}">
				<img src="images/brasseurs/brasseur-${brass.id}.${brass.img}" alt="Photo de ${brass.nom}" />

			</c:if>
		
			Infos : ${brass.nom}<br />
			<a href="${brass.siteWeb}">${brass.siteWeb}</a><br />
			
		</div>
	</div>
</c:forEach>