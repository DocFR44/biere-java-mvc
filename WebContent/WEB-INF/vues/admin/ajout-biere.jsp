<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<h2>Ajouter une bière</h2>

<div id="form-rech-bieres">

<form method="post" action="${pageContext.request.contextPath}/admin/ajout-biere" enctype="multipart/form-data">

<p>${requestScope.modAjoutBiere.msgErreur} ${requestScope.erreurphoto}</p>
<p>${requestScope.BiereAjoute}</p>
<p>

<label for="nomBiere">Nom de la bière : </label>
<c:choose>
	<c:when test="${sessionScope.nomAjout != null}">
		<input type="text" name="nomBiere" id="nomBiere" value="${sessionScope.nomAjout}" />
	</c:when>
	<c:otherwise>
		<input type="text" name="nomBiere" id="nomBiere" />
	</c:otherwise>	
</c:choose>	

</p>

<p>
<label for="nomBrasseur">Nom du brasseur: </label>
<select name="nomBrasseur" id="nomBrasseur">
	<option value="selectionBrasseur">Sélectionner un brasseur</option>
	<c:forEach var="nomBrasseur" items="${requestScope.modBrasseur.lstBrasseurs}">
		<c:choose>
			<c:when test="${sessionScope.brasseurAjout != null}">
				<c:choose>							
					<c:when test="${(sessionScope.brasseurAjout == nomBrasseur.id)}">
						<option value="${nomBrasseur.id}" selected="selected">${nomBrasseur.nom}</option>
					</c:when>
					<c:otherwise>
						<option value="${nomBrasseur.id}">${nomBrasseur.nom}</option>
					</c:otherwise>	
				</c:choose>			
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${(param['nomBrasseur'] == nomBrasseur.id)}">
						<option value="${nomBrasseur.id}" selected="selected">${nomBrasseur.nom}</option>
					</c:when>
					<c:otherwise>
						<option value="${nomBrasseur.id}">${nomBrasseur.nom}</option>
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</select>
</p>

<p>
<label for="description">Description: </label>
<c:choose>
	<c:when test="${sessionScope.descriptionAjout != null}">
		<textarea name="description" id="description">${sessionScope.descriptionAjout}</textarea>
	</c:when>
	<c:otherwise>
		<textarea name="description" id="description"></textarea>
	</c:otherwise>	
</c:choose>	

</p>

<p>
<label for="tauxAlcool">Taux alcool: </label>

<c:choose>
	<c:when test="${sessionScope.tauxAlcoolAjout != null}">
		<input name="tauxAlcool" id="tauxAlcool" type="number" min="0" step="0.5" value="${sessionScope.tauxAlcoolAjout}"/>
	</c:when>
	<c:otherwise>
				<input name="tauxAlcool" id="tauxAlcool" type="number" min="0" step="0.5" />
	</c:otherwise>	
</c:choose>	

</p>

<p>
<label for="categorie">Catégorie : </label>
<select name="categorie" id="categorie">
	<option>Toutes</option>
					
	<c:forEach var="categorie" items="${requestScope.modListCat.lstCategorie}">
		<c:choose>
			<c:when test="${sessionScope.catAjout != null}">
				<c:choose>							
					<c:when test="${(sessionScope.catAjout == categorie.id)}">
						<option value="${categorie.id}" selected="selected">${categorie.nom}</option>
					</c:when>
					<c:otherwise>
						<option value="${categorie.id}">${categorie.nom}</option>
					</c:otherwise>	
				</c:choose>			
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${(param['categorie'] == categorie.id)}">
						<option value="${categorie.id}" selected="selected">${categorie.nom}</option>
					</c:when>
					<c:otherwise>
						<option value="${categorie.id}">${categorie.nom}</option>
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</select>
</p>
<%-- --%> <input type="file" name="photo" />
<p>
			<input type="submit" name="ajout-bieres" value="Ajouter une biere" class="btn" />
		</p>
</form>
</div>