package ca.garneau.deptinfo.bieres.modeles;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.chrono.MinguoChronology;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.NamingException;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;
import javax.swing.text.AbstractDocument.LeafElement;

import org.apache.jasper.tagplugins.jstl.core.If;

import ca.garneau.deptinfo.bieres.classes.Categorie;
import ca.garneau.deptinfo.util.ReqPrepBdUtil;

public class ModeleListCategorie {

	private ArrayList<Categorie> lstCategorie;

	public ArrayList<Categorie> getLstCategorie() {
		return lstCategorie;
	}

	public void ChercherCategorie() throws NamingException, SQLException {
		String reqSQLChercherCategorie = "SELECT id, nom FROM categories ORDER BY nom";
		
		String nomDataSource = "jdbc/bieres";

		ReqPrepBdUtil utilBd = new ReqPrepBdUtil(nomDataSource);
		
		utilBd.ouvrirConnexion();

		utilBd.preparerRequete(reqSQLChercherCategorie, false);

		ResultSet rs = utilBd.executerRequeteSelect();
		
		this.lstCategorie = new ArrayList<Categorie>();
		
		while (rs.next()) {

			this.lstCategorie.add(new Categorie(rs.getString("categories.id"), rs.getString("categories.nom")));
		}

		utilBd.fermerConnexion();
	}
}
