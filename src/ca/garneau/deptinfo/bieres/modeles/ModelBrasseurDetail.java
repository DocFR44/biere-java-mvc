package ca.garneau.deptinfo.bieres.modeles;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.security.auth.message.callback.PrivateKeyCallback.Request;

import ca.garneau.deptinfo.bieres.classes.Brasseur;
import ca.garneau.deptinfo.util.ReqPrepBdUtil;

public class ModelBrasseurDetail {
	
	private ArrayList<Brasseur> lstBrasseurs;
	

	public ArrayList<Brasseur> getLstBrasseurs() {
		return lstBrasseurs;
	}

	public void setLstBrasseurs(ArrayList<Brasseur> lstBrasseurs) {
		this.lstBrasseurs = lstBrasseurs;
	}

	
	public void ChercherBrasseurs() throws NamingException, SQLException {
		
		String reqSQLChercherBrasseur = "SELECT * FROM brasseurs";
		
		String nomDataSource = "jdbc/bieres";

		ReqPrepBdUtil utilBd = new ReqPrepBdUtil(nomDataSource);
		
		utilBd.ouvrirConnexion();

		utilBd.preparerRequete(reqSQLChercherBrasseur, false);

		ResultSet rs = utilBd.executerRequeteSelect();
		this.lstBrasseurs = new ArrayList<Brasseur>();
		Brasseur unBrasseur;
		while (rs.next())
		{
			unBrasseur = new Brasseur(
				rs.getString("id"),
				rs.getString("nom"),
				rs.getString("adresse"),
				rs.getString("ville"),
				rs.getString("etat_prov"),
				rs.getString("code_postal"),
				rs.getString("pays"),
				rs.getString("telephone"),
				rs.getString("site_web"),
				rs.getString("description"),
				rs.getString("image"));
			this.lstBrasseurs.add(unBrasseur);
		}
		utilBd.fermerConnexion();
	}

}
