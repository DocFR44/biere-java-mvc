package ca.garneau.deptinfo.bieres.modeles;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.naming.NamingException;
import ca.garneau.deptinfo.util.ReqPrepBdUtil;

public class ModelCritique {
	private String msgErreur;

	public String getMsgErreur() {
		return msgErreur;
	}

	public void setMsgErreur(String msgErreur) {
		this.msgErreur = msgErreur;
	}
	
	public void ajouterCritique(String user_id, String biere_id, String arome, String apparences, String gout, String style_bouteille, String general) throws NamingException, SQLException
	{
		String reqSQLAjoutCritique = "INSERT INTO critiques (date_creation, user_id, biere_id, arome, apparence, gout, style_bouteille, general) VALUES (?,?,?,?,?,?,?,?)";
		ArrayList<Object> params = new ArrayList<Object>();
		String nomDataSource = "jdbc/bieres";
		

		Date d = new Date();
		params.add(d);
		
		if(user_id != null)
		{
			params.add(user_id);
		}
		else
		{
			setMsgErreur("Il faut être connecté pour ajouter une critique");
			return;
		}
		
		if(biere_id != null)
		{
			params.add(biere_id);
		}
		else
		{
			setMsgErreur("Veuillez sélectionner une bière.");
			return;
		}
		
		int nombreEntier;
		
		if(!arome.equals(""))
		{
			if(EstUnNombre(arome))
			{
				nombreEntier = Integer.parseInt(arome); 
				if(nombreEntier > 0 && nombreEntier < 11)
				{
					params.add(nombreEntier);
				}
				else
				{
					setMsgErreur("Veuillez entrer un nombre entre 1 et 10 pour l'arome");
					return;
				}
			}
			else
			{
				setMsgErreur("Veuillez entrer un nombre pour l'arome");
				return;
			}
		}
		else
		{
			setMsgErreur("Veuillez entrer une note pour l'arome");
			return;
		}
		
		if(!apparences.equals(""))
		{
			if(EstUnNombre(apparences))
			{
				nombreEntier = Integer.parseInt(apparences); 
				if(nombreEntier > 0 && nombreEntier < 11)
				{
					params.add(nombreEntier);
				}
				else
				{
					setMsgErreur("Veuillez entrer un nombre entre 1 et 10 pour l'apparences");
					return;
				}
			}
			else
			{
				setMsgErreur("Veuillez entrer un nombre pour l'apparences");
				return;
			}
		}
		else
		{
			setMsgErreur("Veuillez entrer une note pour l'apparences");
			return;
		}
		
		if(!gout.equals(""))
		{
			if(EstUnNombre(gout))
			{
				nombreEntier = Integer.parseInt(gout); 
				if(nombreEntier > 0 && nombreEntier < 11)
				{
					params.add(nombreEntier);
				}
				else
				{
					setMsgErreur("Veuillez entrer un nombre entre 1 et 10 pour le gout");
					return;
				}
			}
			else
			{
				setMsgErreur("Veuillez entrer un nombre pour le gout");
				return;
			}
		}
		else
		{
			setMsgErreur("Veuillez entrer une note pour le gout");
			return;
		}
		
		if(!style_bouteille.equals(""))
		{
			if(EstUnNombre(gout))
			{
				nombreEntier = Integer.parseInt(style_bouteille); 
				if(nombreEntier > 0 && nombreEntier < 11)
				{
					params.add(nombreEntier);
				}
				else
				{
					setMsgErreur("Veuillez entrer un nombre entre 1 et 10 pour le style de la bouteille");
					return;
				}
			}
			else
			{
				setMsgErreur("Veuillez entrer un nombre pour le style de la bouteille");
				return;
			}
		}
		else
		{
			setMsgErreur("Veuillez entrer une note pour le style de la bouteille");
			return;
		}
		
		if(!general.equals(""))
		{
			if(EstUnNombre(gout))
			{
				nombreEntier = Integer.parseInt(general); 
				if(nombreEntier > 0 && nombreEntier < 11)
				{
					params.add(nombreEntier);
				}
				else
				{
					setMsgErreur("Veuillez entrer un nombre entre 1 et 10 pour le style general");
					return;
				}
			}
			else
			{
				setMsgErreur("Veuillez entrer un nombre pour le style general");
				return;
			}
		}
		else
		{
			setMsgErreur("Veuillez entrer une note pour le style general");
			return;
		}
		
		if(getMsgErreur() == null){
			ReqPrepBdUtil utilBd = new ReqPrepBdUtil(nomDataSource);
			
			utilBd.ouvrirConnexion();

			utilBd.preparerRequete(reqSQLAjoutCritique, false);
			
			utilBd.executerRequeteMaj(params.toArray());
			
		
			utilBd.fermerConnexion();
		}
		
		
	}
	
	private static boolean EstUnNombre(String str)  
	{  
	  try  
	  {  
	    int i = Integer.parseInt(str); 
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}

}
