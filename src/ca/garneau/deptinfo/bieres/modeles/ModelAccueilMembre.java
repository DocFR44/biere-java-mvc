package ca.garneau.deptinfo.bieres.modeles;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.NamingException;

import ca.garneau.deptinfo.bieres.classes.Biere;
import ca.garneau.deptinfo.bieres.classes.Categorie;
import ca.garneau.deptinfo.bieres.classes.Critique;
import ca.garneau.deptinfo.util.ReqPrepBdUtil;

public class ModelAccueilMembre {
	// Attributs
		// =========
				
		private ArrayList<Critique> lstCritiqueTrouvees;
		private String msgErreur;

		
		// Constructeur
		// ============

		public ModelAccueilMembre() {
			this.lstCritiqueTrouvees = null;
		}

		
		// Getters et Setters
		// ==================
		public List<Critique> getlstCritiqueTrouvees() {
			return this.lstCritiqueTrouvees;
		}
		public void setlstCritiqueTrouvees(ArrayList<Critique> lstCritiqueTrouvees) {
			this.lstCritiqueTrouvees = lstCritiqueTrouvees;
		}
		
		public String getMsgErreur()
		{
			return this.msgErreur;
		}
		public void setMsgErreur(String msg)
		{
			this.msgErreur = msg;
		}



		
	
public void rechercheCritique(String user_id) throws NamingException, SQLException {
	ArrayList<Object> params = new ArrayList<Object>();
	String reqSQLChercherCategorie = "SELECT critiques.id, bieres.nom, critiques.date_creation, critiques.arome, critiques.apparence, critiques.gout, critiques.style_bouteille, critiques.general "
			+ "							FROM critiques "
			+ "							INNER JOIN bieres "
			+ "							ON bieres.id = critiques.biere_id "
			+ "							WHERE critiques.user_id = ? "
			+ "							ORDER BY critiques.date_creation DESC "
			+ "							LIMIT 10 ";
	
	params.add(user_id);
										
	
	String nomDataSource = "jdbc/bieres";

	ReqPrepBdUtil utilBd = new ReqPrepBdUtil(nomDataSource);
	
	utilBd.ouvrirConnexion();

	utilBd.preparerRequete(reqSQLChercherCategorie, false);

	ResultSet rs = utilBd.executerRequeteSelect(params.toArray());

	this.lstCritiqueTrouvees = new ArrayList<Critique>();
	
	while (rs.next()) {
		Date dateCrea = rs.getDate("critiques.date_creation");
		this.lstCritiqueTrouvees.add(new Critique(dateCrea.toString(), rs.getString("bieres.nom"), rs.getInt("critiques.arome"), rs.getInt("critiques.gout"), rs.getInt("critiques.apparence"), rs.getInt("critiques.style_bouteille"), rs.getInt("critiques.general")));
	}

	utilBd.fermerConnexion();
	}
}
