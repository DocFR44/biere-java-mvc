package ca.garneau.deptinfo.bieres.modeles;

import java.security.MessageDigest;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;


import ca.garneau.deptinfo.bieres.beans.ConnexionBean;
import ca.garneau.deptinfo.bieres.classes.ConnexionMode;
import ca.garneau.deptinfo.util.ReqPrepBdUtil;


public class ModelConnexion {

	private ConnexionBean utilConnecte;
	
	private String msgErreur;

	public ConnexionBean getUtilConnecte() {
		return utilConnecte;
	}

	public void setUtilConnecte(ConnexionBean utilConnecte) {
		this.utilConnecte = utilConnecte;
	}

	public String getMsgErreur() {
		return msgErreur;
	}

	public void setMsgErreur(String msgErreur) {
		this.msgErreur = msgErreur;
	}
	
	public void ChercherUtilisateur(String user, String mdp, Boolean estEmploye) throws NamingException, SQLException 
	{
		user = user.trim();
		if(user != "" && mdp != "")
		{
			ArrayList<Object> params = new ArrayList<Object>();
			String mdpHash = sha256(mdp);
			
			String reqSQLChercherUtilisateur;
			
			if(estEmploye)
			{
				reqSQLChercherUtilisateur = "SELECT * FROM employes WHERE no = ? AND password = ?";
				params.add(user);
				params.add(mdpHash);
			}
			else
			{
				reqSQLChercherUtilisateur = "SELECT * FROM membres WHERE username = ? AND password = ?";
				params.add(user);
				params.add(mdpHash);
			}
			
			
			String nomDataSource = "jdbc/bieres";
	
			ReqPrepBdUtil utilBd = new ReqPrepBdUtil(nomDataSource);
			
			utilBd.ouvrirConnexion();
	
			utilBd.preparerRequete(reqSQLChercherUtilisateur, false);
	
			ResultSet rs = utilBd.executerRequeteSelect(params.toArray());
	
	
			
			if(!rs.next())
			{
				this.msgErreur = "Erreur dans le nom d'utilisateur ou de mot de passe";
				this.utilConnecte = null;
				return;
			}
			else
			{
				this.msgErreur = null;
				this.utilConnecte = new ConnexionBean();

				if(estEmploye)
				{
					this.utilConnecte.setNomUtil(rs.getString("nom")+ " " + rs.getString("prenom") );
					this.utilConnecte.setNoUtil(rs.getInt("no"));

					this.utilConnecte.setNom(rs.getString("nom"));
					this.utilConnecte.setModeConn(ConnexionMode.ADMIN);
				}
				else
				{
					this.utilConnecte.setNoUtil(rs.getInt("id"));
					this.utilConnecte.setNom(rs.getString("username"));
					this.utilConnecte.setModeConn(ConnexionMode.MEMBRE);
				}
				
			}
				
			utilBd.fermerConnexion();
		}
		else
		{
			this.msgErreur = "Veuillez bien remplir les champs";
			this.utilConnecte = null;
		}
	}



/*http://stackoverflow.com/questions/5531455/how-to-hash-some-string-with-sha256-in-java*/
	public static String sha256(String base) {
	    try{
	        MessageDigest digest = MessageDigest.getInstance("SHA-256");
	        byte[] hash = digest.digest(base.getBytes("UTF-8"));
	        StringBuffer hexString = new StringBuffer();
	
	        for (int i = 0; i < hash.length; i++) {
	            String hex = Integer.toHexString(0xff & hash[i]);
	            if(hex.length() == 1) hexString.append('0');
	            hexString.append(hex);
	        }
	
	        return hexString.toString();
	    } catch(Exception ex){
	       throw new RuntimeException(ex);
	    }
	}
}
