package ca.garneau.deptinfo.bieres.modeles;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import ca.garneau.deptinfo.bieres.classes.Biere;
import ca.garneau.deptinfo.util.ReqPrepBdUtil;

/**
 * Modèle pour la recherche de bières.
 * Permet de valider les paramètres de la requête et si ceux-ci sont valides
 * de créer la requête SQL pour la recherche de bières (la requête n'est pas exécutée ici)
 * ainsi que le bean permettant de conserver ces paramètres valides dans la session utilisateur.
 * @author Stéphane Lapointe
 */
public class ModeleRechBieres {
	

	// Attributs
	// =========
			
	/**
	 * Liste des bière trouvées.
	 */
	private ArrayList<Biere> lstBieresTrouvees;
	private String msgErreur;
	private int nbrBiere;
	public int getnbrBiere()
	{
		return this.nbrBiere;
	}
	public void setnbrBiere(int nbr)
	{
		this.nbrBiere = nbr;
	}
	
	// Constructeur
	// ============
	/**
	 * Initialise les attributs du modèle de recherche de bières.
	 */
	public ModeleRechBieres() {
		this.lstBieresTrouvees = null;
	}

	
	// Getters et Setters
	// ==================

	/**
	 * Retourne la liste des bières trouvées.
	 * @return La liste des bières trouvées.
	 */
	public List<Biere> getLstBieresTrouvees() {
		return this.lstBieresTrouvees;
	}
	
	public String getMsgErreur()
	{
		return this.msgErreur;
	}
	public void setMsgErreur(String msg)
	{
		this.msgErreur = msg;
	}

	/**
	 * Modifie la liste des bières trouvées.
	 * @param lstBieresTrouvees La nouvelle liste des bières trouvées.
	 */
	public void setLstBieresTrouvees(ArrayList<Biere> lstBieresTrouvees) {
		this.lstBieresTrouvees = lstBieresTrouvees;
	}
	

	
	// Méthodes
	// ========
	
	/**
	 * Permet de rechercher les bières correspondant aux critères de recherche.
	 * @param motCle : mot clé.
	 * @param categorie : catégorie de la bière.
	 * @throws SQLException S'il y a une erreur SQL quelconque.
	 * @throws NamingException 
	 */
	public void rechercherBieres(String motCle, String categorie, String min, String max, String pagination) throws NamingException, SQLException {

		
		final String TOUTES = "Toutes";
		if (motCle != null) {
			motCle = motCle.trim();
			motCle = motCle.replaceAll("[']", "''");	
		}

		// Catégorie
		if (!categorie.equals(TOUTES)) {
			categorie = categorie.replaceAll("[']", "''");
		}
					
		
		if (motCle != null || !categorie.equals(TOUTES) || min != null || max!= null) {

			// Création de la requête SQL
			ArrayList<String> conditions = new ArrayList<String>();
			
			ArrayList<Object> params = new ArrayList<Object>();

			if (motCle != null) {
				conditions.add("( bieres.nom LIKE ? OR bieres.description LIKE ? )");
				params.add("%" + motCle + "%");
				params.add("%" + motCle + "%");
			}

			if (!categorie.equals(TOUTES)) {
				conditions.add("cat_id = ?");
				params.add(categorie);
			}
			

			
			if (min != "")
			{
				if(EstUnNombre(min))
				{
					float fMin = Float.parseFloat(min);
					if(fMin >=0)
					{
					conditions.add("(abv >= ?)");
					params.add(min);
					}
					else{
						setMsgErreur("La valeur du taux d'alcool doit être un nombre positif.");
						return;
					}
				}
				else{
					setMsgErreur("La valeur du taux d'alcool doit être un nombre positif");
					return;
				}
			}
			
			
			if (max != "")
			{
				if(EstUnNombre(max))
				{
					float fMax = Float.parseFloat(max);
					if(fMax >=0)
					{
					conditions.add("(abv <= ?)");
					params.add(max);
					}
					else{
						setMsgErreur("La valeur du taux d'alcool doit être un nombre positif.");
						return;
					}
				}
				else{
					setMsgErreur("La valeur du taux d'alcool doit être un nombre positif");
					return;
				}
			}
			




			String reqSQLRechBieres = "SELECT bieres.id, bieres.brasseur_id, bieres.nom, bieres.cat_id,"
					+ " bieres.style_id, bieres.abv, bieres.description, bieres.image, bieres.derniere_modification, "
					+ "brasseurs.nom, categories.nom "
					+ "FROM categories "
					+ "INNER JOIN bieres ON bieres.cat_id = categories.id "
					+ "INNER JOIN brasseurs ON bieres.brasseur_id = brasseurs.id ";
			
			String reqSQLRechBieresCount = "SELECT Count(*) as nbr FROM bieres";
			if (!conditions.isEmpty()) {
				reqSQLRechBieres += " WHERE " + conditions.get(0);
				reqSQLRechBieresCount += " WHERE " + conditions.get(0);
				for (int i = 1; i < conditions.size(); i++) {
					reqSQLRechBieres += " AND " + conditions.get(i);
					reqSQLRechBieresCount += " AND " + conditions.get(i);
				}
			}

			
			reqSQLRechBieres += " ORDER BY bieres.nom";

			if(pagination == null)
			{
				reqSQLRechBieres += " LIMIT 10";
			}
			else
			{
				reqSQLRechBieres += " LIMIT 10 OFFSET "+ (Integer.parseInt(pagination)-1) *10;
			}

			String nomDataSource = "jdbc/bieres";


			ReqPrepBdUtil utilBd = new ReqPrepBdUtil(nomDataSource);

			utilBd.ouvrirConnexion();

			utilBd.preparerRequete(reqSQLRechBieres, false);
			

			ResultSet rs = utilBd.executerRequeteSelect(params.toArray());
			
			utilBd.preparerRequete(reqSQLRechBieresCount, false);
			ResultSet rsCount = utilBd.executerRequeteSelect(params.toArray());
			
			rsCount.next();
			setnbrBiere(rsCount.getInt("nbr"));
			
			
			this.lstBieresTrouvees = new ArrayList<Biere>();

			Biere biere;
			
			while (rs.next()) {
				biere = new Biere(
						rs.getInt("bieres.id"),
						rs.getInt("bieres.brasseur_id"),
						rs.getString("brasseurs.nom"),
						rs.getString("bieres.nom"),
						rs.getInt("bieres.cat_id"),
						rs.getString("categories.nom"),
						rs.getInt("bieres.style_id"),
						rs.getInt("bieres.abv"),
						rs.getDate("bieres.derniere_modification"),
						rs.getString("bieres.description"),
						rs.getString("bieres.image"));

				this.lstBieresTrouvees.add(biere);
			}

			utilBd.fermerConnexion();
			
		}
			

}
	private static boolean EstUnNombre(String str)  
	{  
	  try  
	  {  
	    float f = Float.parseFloat(str); 
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
	


}
