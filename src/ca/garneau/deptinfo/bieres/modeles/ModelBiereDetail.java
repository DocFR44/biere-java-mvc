package ca.garneau.deptinfo.bieres.modeles;

import java.sql.Date;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.naming.NamingException;

import com.mysql.fabric.xmlrpc.base.Param;

import ca.garneau.deptinfo.bieres.classes.Biere;
import ca.garneau.deptinfo.bieres.classes.Brasseur;
import ca.garneau.deptinfo.util.ReqPrepBdUtil;

public class ModelBiereDetail {
	private Biere uneBiere;
	
	public Biere getUneBiere() {
		return uneBiere;
	}

	public void setUneBiere(Biere uneBiere) {
		this.uneBiere = uneBiere;
	}

	public void ChercherBiere(String idBiere) throws NamingException, SQLException {
		
		String reqSQLChercherBiere = "SELECT bieres.id, bieres.brasseur_id, bieres.nom, bieres.cat_id,"
				+ " bieres.style_id, bieres.abv, bieres.description, bieres.image, bieres.derniere_modification, "
				+ "brasseurs.nom, categories.nom "
				+ "FROM categories "
				+ "INNER JOIN bieres ON bieres.cat_id = categories.id "
				+ "INNER JOIN brasseurs ON bieres.brasseur_id = brasseurs.id WHERE bieres.id = ?";
		
		ArrayList<Object> params = new ArrayList<Object>();
		
		params.add(idBiere);
		String nomDataSource = "jdbc/bieres";

		ReqPrepBdUtil utilBd = new ReqPrepBdUtil(nomDataSource);
		
		utilBd.ouvrirConnexion();

		utilBd.preparerRequete(reqSQLChercherBiere, false);

		ResultSet rs = utilBd.executerRequeteSelect(params.toArray());
		rs.next();
		this.uneBiere = new Biere(
				rs.getInt("bieres.id"),
				rs.getInt("bieres.brasseur_id"),
				rs.getString("brasseurs.nom"),
				rs.getString("bieres.nom"),
				rs.getInt("bieres.cat_id"),
				rs.getString("categories.nom"),
				rs.getInt("bieres.style_id"),
				rs.getInt("bieres.abv"),
				rs.getDate("bieres.derniere_modification"),
				rs.getString("bieres.description"),
				rs.getString("bieres.image"));
	
		utilBd.fermerConnexion();
	}

	
}
