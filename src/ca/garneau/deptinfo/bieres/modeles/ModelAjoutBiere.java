package ca.garneau.deptinfo.bieres.modeles;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.naming.NamingException;

import org.apache.tomcat.util.http.fileupload.FileItem;

import ca.garneau.deptinfo.util.ReqPrepBdUtil;

public class ModelAjoutBiere {
	
	private String msgErreur;
	private String nomFichier;
	public String getnomFichier()
	{
		return this.nomFichier;
	}
	public void setnomFichier(String nom)
	{
		this.nomFichier = nom;
	}
	public String getMsgErreur()
	{
		return this.msgErreur;
	}
	public void setMsgErreur(String msg)
	{
		this.msgErreur = msg;
	}
	
	public void ajouterBiere(String idBrasseur, String nomBiere, String idCategorie, String abv, String description, FileItem itemphoto) throws NamingException, SQLException
	{
		String reqSQLChercherBrasseur = "INSERT INTO Bieres (nom, brasseur_id, description, abv, cat_id, derniere_modification, image) VALUES (?,?,?,?,?,?,?)";
		ArrayList<Object> params = new ArrayList<Object>();
		String nomDataSource = "jdbc/bieres";
		

		Date d = new Date();
		
		//test nom biere
		if(!nomBiere.equals(""))
		{
		params.add(nomBiere);
		}
		else
		{
			setMsgErreur("Veuillez entrer un nom de bière");
			return;
		}
		
		//test idbrasseur
		if(EstUnNombre(idBrasseur))
		{
		params.add(Integer.parseInt(idBrasseur));
		}
		else
		{
			setMsgErreur("Veillez selectionner un brasseur");
			return;
		}
		
		//test description
		if(!description.equals(""))
		{
		params.add(description);
		}
		else
		{
			setMsgErreur("Veuillez entrer une description");
			return;
		}
		
		//test tauxalcool
		if(!abv.equals(""))
		{
			if(EstUnNombre(abv))
			{
				params.add(Float.parseFloat(abv));
			}
			else
			{
				setMsgErreur("le taux d'alcool doit être un nombre");
				return;
			}
		}
		else
		{
			setMsgErreur("Veuillez entrer un taux d'alcool");
			return;
		}
		
		//test categorie
		if(EstUnNombre(idCategorie))
		{
		params.add(Integer.parseInt(idCategorie));
		}
		else
		{
			setMsgErreur("Veillez selectionner une categorie");
			return;
		}
		
		params.add(d);
		
		uploadphoto(itemphoto);
		if(getMsgErreur() == null)
		{	
		params.add("images/bieres/" + getnomFichier());
		
		
		ReqPrepBdUtil utilBd = new ReqPrepBdUtil(nomDataSource);
		
		utilBd.ouvrirConnexion();

		utilBd.preparerRequete(reqSQLChercherBrasseur, false);
		
		utilBd.executerRequeteMaj(params.toArray());
		
	
		utilBd.fermerConnexion();
		}
		
	}
	
	private static boolean EstUnNombre(String str)  
	{ 
	  try  
	  {  
	    float f = Float.parseFloat(str); 
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
	
	private void uploadphoto(FileItem item)
	{
	    String contentType = item.getContentType();
	    if(!contentType.equals("image/png") && !contentType.equals("image/jpeg")){
	    	setMsgErreur("Vous devez selectionner une image de type jpg ou png");
	    	return;
	    } 
	    else
	    {
////////////////////////System.getProperty("user.dir")) On sait que c'est ça, mais ça marche pas :P
	    File uploadDir = new File("C:/Users/ELMO/Documents/annee3/session5/web-avance/TPs/TP1/TP1-Hamel-Marion/WebContent/images/bieres");
///////////////////////
	    
	    File file;
		    
	    try {
	    if(contentType.equals("image/png"))
		    {
		    file = File.createTempFile("imgage",".png",uploadDir);
		    }else{
		    file = File.createTempFile("imgage",".jpg",uploadDir);
		    }
		} catch (Exception e) {
			setMsgErreur("Vous devez selectionner une image de type jpg ou png");
			return;
		}

	    	try {
				item.write(file);
			} catch (Exception e) {
				setMsgErreur("Erreur à l'écriture du fichier");
				e.printStackTrace();
			}
	    	setnomFichier(file.getName());
		    	
		    
	    }
	}

}
