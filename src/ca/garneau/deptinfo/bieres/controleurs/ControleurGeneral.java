package ca.garneau.deptinfo.bieres.controleurs;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ca.garneau.deptinfo.bieres.modeles.ModelBiereDetail;
import ca.garneau.deptinfo.bieres.modeles.ModelBrasseurDetail;
import ca.garneau.deptinfo.bieres.modeles.ModelConnexion;
import ca.garneau.deptinfo.bieres.modeles.ModeleListCategorie;
import ca.garneau.deptinfo.bieres.modeles.ModeleRechBieres;

/**
 * Contrôleur général pour les ressources publiques.
 * @author Stéphane Lapointe
 * @author VOS NOMS COMPLETS ICI
 */
public class ControleurGeneral extends HttpServlet {
private static final long serialVersionUID = 1L;
	
	// Attributs
	// =========
	/**
	 * URI sans le context path.
	 */
	protected static String uri;
	
	/**
	 * Vue à afficher (chemin du fichier sur le serveur).
	 */
	protected static String vue;
	
	/**
	 * Fragment de la vue (chemin du fichier sur le serveur) à charger
	 * dans la zone de contenu si la vue est créée à partir du gabarit.
	 */
	protected static String vueContenu;
	
	/**
	 * Sous-titre de la vue si la vue est créée à partir du gabarit.
	 */
	protected static String vueSousTitre;
	
	/**
	 * Permet d'effectuer les traitements avant de gérer la ressource demandée.
	 * @param request La requête HTTP.
	 * @param response La réponse HTTP.
	 */
	protected static void preTraitement(HttpServletRequest request) {
		// Récupération de l'URI sans le context path.
		ControleurGeneral.uri = request.getRequestURI().substring(request.getContextPath().length());
		ControleurGeneral.vue = null;
		ControleurGeneral.vueContenu = null;
		ControleurGeneral.vueSousTitre = null;		
	}



	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
	 * @param request servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		preTraitement(request);

		if (ControleurGeneral.uri.equals("/rech-bieres") || ControleurGeneral.uri.equals("/") || ControleurGeneral.uri.equals("")) {
			pageRechercheBiereGet(request);

		} else if (ControleurGeneral.uri.equals("/details-biere")) {
			pageDetailBiere(request);
		} 
		
		else if (ControleurGeneral.uri.equals("/brasseurs")) {	
			pageBrasseurs(request);

		} else if (ControleurGeneral.uri.equals("/connexion")) {
			response.sendError(405);
			
		}else if (ControleurGeneral.uri.equals("/deconnexion")){
			pageDeconnexion(request);
		}
		else {
			response.sendError(404);	
		}

		postTraitement(request, response);

	}

	/**
	 * Permet de gérer les ressources POST suivantes :
	 * 		"/connexion"	:	Connexion
	 * 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		preTraitement(request);

		if (ControleurGeneral.uri.equals("/connexion")) {
			pageConnexion(request, response);			
		} else if (ControleurGeneral.uri.equals("/") || ControleurGeneral.uri.equals("") || ControleurGeneral.uri.equals("/rech-bieres") ) {
			response.sendError(405);

		} else {
			response.sendError(404);

		} 
		postTraitement(request, response);
	}


	/**
	 * Permet d'effectuer les traitements suite à la gestion de la ressource demandée.
	 * @param request La requête HTTP.
	 * @param response La réponse HTTP.
	 */
	protected static void postTraitement(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Doit-on transférer le contrôle vers une vue ?
		if (ControleurGeneral.vue != null) {
			// Doit-on conserver les informations pour la production d'une vue à partir du gabarit ?
			if (ControleurGeneral.vueContenu != null || ControleurGeneral.vueSousTitre != null) {
				request.setAttribute("vueContenu", ControleurGeneral.vueContenu);
				request.setAttribute("vueSousTitre", ControleurGeneral.vueSousTitre);
			}
			// Transfert du contrôle de l'exécution à la vue.
			request.getRequestDispatcher(ControleurGeneral.vue).forward(request, response);
		}
	}
	
	private static void pageRechercheBiereGet(HttpServletRequest request)throws ServletException, IOException 
	{
		ModeleRechBieres mrb = new ModeleRechBieres();
		ModeleListCategorie mlc = new ModeleListCategorie();

		if (request.getParameter("rech-bieres") != null)
		{
			request.getSession().setAttribute("motCleRech", request.getParameter("mot-cle"));
			request.getSession().setAttribute("catRech", request.getParameter("categorie"));
			request.getSession().setAttribute("alcMinRech", request.getParameter("tauxAlcoolMin"));
			request.getSession().setAttribute("alcMaxRech", request.getParameter("tauxAlcoolMax"));
			try {
				mrb.rechercherBieres(
						request.getParameter("mot-cle"),
						request.getParameter("categorie"),
						request.getParameter("tauxAlcoolMin"),
						request.getParameter("tauxAlcoolMax"),
						request.getParameter("page")
						);
			} catch (NamingException | SQLException e) {
				throw new ServletException(e);
			}	
			
		}
		try {
		mlc.ChercherCategorie();
		} catch (NamingException | SQLException e) {
		throw new ServletException(e);
		}
		
		request.setAttribute("modRechBieres", mrb);
		request.setAttribute("modListCat", mlc);
		ControleurGeneral.vue = "/WEB-INF/vues/gabarit-vues.jsp";
		ControleurGeneral.vueContenu = "/WEB-INF/vues/general/rech-bieres.jsp";
		ControleurGeneral.vueSousTitre = "Rechercher des bière";
	}

	private static void pageDetailBiere(HttpServletRequest request)throws ServletException, IOException 
	{
		ModelBiereDetail mbd = new ModelBiereDetail();
		
		if (request.getParameter("noBiere") != null)
		{
			try 
			{
				mbd.ChercherBiere(request.getParameter("noBiere"));
			}
			catch (NamingException | SQLException e) {
				throw new ServletException(e);
			}	
		}
		request.setAttribute("modBiereDetail", mbd);
		ControleurGeneral.vue = "/WEB-INF/vues/gabarit-vues.jsp";
		ControleurGeneral.vueContenu = "/WEB-INF/vues/general/biere.jsp";
		ControleurGeneral.vueSousTitre = "Détails d'une bière";
		
	}
	
	private static void pageBrasseurs(HttpServletRequest request)throws ServletException, IOException 
	{
		ModelBrasseurDetail mbrd = new ModelBrasseurDetail();
		
		try 
		{
			mbrd.ChercherBrasseurs();
		}
		catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}	
		
		request.setAttribute("modBrasseur", mbrd);
		ControleurGeneral.vue = "/WEB-INF/vues/gabarit-vues.jsp";
		ControleurGeneral.vueContenu = "/WEB-INF/vues/general/brasseurs.jsp";
		ControleurGeneral.vueSousTitre = "Les brasseurs de bière";
	}
	
	private static void pageDeconnexion(HttpServletRequest request)
	{
		request.getSession().removeAttribute("util");
		if(request.getParameter("btnDeconnexion") != null)
		{
			request.setAttribute("messageConfirmation", "Vous êtes maintenant déconnecté");
		}
		ControleurGeneral.vue = "/WEB-INF/vues/gabarit-vues.jsp";
		ControleurGeneral.vueContenu = "/WEB-INF/vues/general/rech-bieres.jsp";
		ControleurGeneral.vueSousTitre = "Page d'accueil";
	}
	
	private static void pageConnexion(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
	{
		Boolean isChecked;
		ModelConnexion mc= new ModelConnexion();
		try 
		{
			request.setAttribute("username", request.getParameter("identifiant"));
			if (request.getParameter("typeConn") == null)
			{
				isChecked = false;
			}
			else
			{
				isChecked = true;
				request.setAttribute("isChecked", "oui");
			}
			mc.ChercherUtilisateur(request.getParameter("identifiant"), request.getParameter("motPasse"), isChecked);
		}
		catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}	
		
		if(mc.getMsgErreur() == null)
		{

			request.getSession().setAttribute("util",  mc.getUtilConnecte());
			
			if (isChecked)
			{
				response.sendRedirect("admin");
				
			}
			else
			{
				response.sendRedirect("membre");
			}
		}
		else
		{				
			request.setAttribute("msgErreur", mc.getMsgErreur());

			ModeleListCategorie mlc = new ModeleListCategorie();
			try{
			mlc.ChercherCategorie();
			} catch (NamingException | SQLException e) {
				throw new ServletException(e);
				}
			
			
			request.setAttribute("modListCat", mlc);
			ControleurGeneral.vue = "/WEB-INF/vues/gabarit-vues.jsp";
			ControleurGeneral.vueContenu = "/WEB-INF/vues/general/rech-bieres.jsp";
			ControleurGeneral.vueSousTitre = "Page d'accueil";
		}
	}
}
