package ca.garneau.deptinfo.bieres.controleurs;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ca.garneau.deptinfo.bieres.beans.ConnexionBean;
import ca.garneau.deptinfo.bieres.classes.ConnexionMode;
import ca.garneau.deptinfo.bieres.modeles.ModelAjoutBiere;

import ca.garneau.deptinfo.bieres.modeles.ModelBrasseurDetail;
import ca.garneau.deptinfo.bieres.modeles.ModeleListCategorie;
import java.util.List;
 

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

import javax.servlet.annotation.MultipartConfig;


/**
 * Contrôleur-répartiteur pour la section réservée au personnel (staff).
 * @author Stéphane Lapointe
 */
@MultipartConfig
public class ControleurAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	// Attributs
	// =========
	/**
	 * URI sans le context path.
	 */
	protected static String uri;
	
	/**
	 * Vue à afficher (chemin du fichier sur le serveur).
	 */
	protected static String vue;
	
	/**
	 * Fragment de la vue (chemin du fichier sur le serveur) à charger
	 * dans la zone de contenu si la vue est créée à partir du gabarit.
	 */
	protected static String vueContenu;
	
	/**
	 * Sous-titre de la vue si la vue est créée à partir du gabarit.
	 */
	protected static String vueSousTitre;
	
	/**
	 * Permet d'effectuer les traitements avant de gérer la ressource demandée.
	 * @param request La requête HTTP.
	 * @param response La réponse HTTP.
	 * @return true si les opérations se sont bien déroulées; false, autrement.
	 * @throws IOException 
	 */
	protected static boolean preTraitement(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// Récupération de l'URI sans le context path.
		ControleurAdmin.uri = request.getRequestURI().substring(request.getContextPath().length());
		ControleurAdmin.vue = null;
		ControleurAdmin.vueContenu = null;
		ControleurAdmin.vueSousTitre = null;
		
		// Expiration de la cache pour les pages de cette section.		
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");  // HTTP 1.1.
		response.setHeader("Pragma", "no-cache");  // HTTP 1.0.
		response.setDateHeader("Expires", 0);  // Proxies.

		ConnexionBean unUser = (ConnexionBean)request.getSession().getAttribute("util"); 
		
		// Contrôle d'accès à la section pour les employés.
		if (unUser == null || unUser.getModeConn() != ConnexionMode.ADMIN) {
			// Non connecté en tant qu'admin; on retourne un code d'erreur
			// HTTP 401 qui sera intercepté par la page d'erreur "erreur-401.jsp".
			response.sendError(401);
			return false;
		}
		else
			return true;
	}

	/**
	 * Permet de gérer les ressources GET suivantes :
	 * 		"/admin/" ou "/admin"		:	Accueil pour le personnel
	 *		"/admin/critiques"			:	Liste des critiques
	 *		"/admin/ajout-biere"		:	Ajout d'une bière
	 * 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// Opérations pré-traitement et suite des opérations, si nécessaire.
		if (preTraitement(request, response)) {

			// ================================
			// Gestion de la ressource demandée
			// ================================

			// Accueil - Employés
			// =================
			if (uri.equals("/admin/") || uri.equals("/admin")) {
				pageAccueilAdminGET(request);
			} else if (uri.equals("/admin/ajout-biere")) {
				pageAjoutBiereAdminGET(request);
			}else {
				response.sendError(404);
			}
			
		}
		postTraitement(request, response);

	}

	/**
	 * Permet de gérer les ressources POST suivantes :
	 * 		Aucune ressource.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if (preTraitement(request, response)) {
			
			if (uri.equals("/admin/") || uri.equals("/admin")) {
				response.sendError(405);

			} 
			else if (uri.equals("/admin/ajout-biere")) {

				postAjoutBiereAdmin(request);		
			}
			else {
				response.sendError(404);
			} 	
		}

		postTraitement(request, response);

	}
	
	/**
	 * Permet d'effectuer les traitements suite à la gestion de la ressource demandée.
	 * @param request La requête HTTP.
	 * @param response La réponse HTTP.
	 */
	protected static void postTraitement(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (ControleurAdmin.vue != null) {
			if (ControleurAdmin.vueContenu != null || ControleurAdmin.vueSousTitre != null) {
				request.setAttribute("vueContenu", ControleurAdmin.vueContenu);
				request.setAttribute("vueSousTitre", ControleurAdmin.vueSousTitre);
			}
			request.getRequestDispatcher(ControleurAdmin.vue).forward(request, response);
		}		
	}	

	private static void pageAccueilAdminGET(HttpServletRequest request)
	{
		ControleurAdmin.vue = "/WEB-INF/vues/gabarit-vues.jsp";
		ControleurAdmin.vueContenu = "/WEB-INF/vues/admin/accueil-admin.jsp";
		ConnexionBean unUser = (ConnexionBean)request.getSession().getAttribute("util"); 
		ControleurAdmin.vueSousTitre = "Page personnelle de " + unUser.getNomUtil() + " (" + unUser.getNoUtil() + ")";
	}
	
	private static void pageAjoutBiereAdminGET(HttpServletRequest request)throws ServletException, IOException
	{
		ModelBrasseurDetail mrb = new ModelBrasseurDetail();
		ModeleListCategorie mlc = new ModeleListCategorie();
		request.getSession().setAttribute("nomAjout", request.getParameter("nomBiere"));
		request.getSession().setAttribute("brasseurAjout", request.getParameter("nomBrasseur"));
		request.getSession().setAttribute("descriptionAjout", request.getParameter("description"));
		request.getSession().setAttribute("tauxAlcoolAjout", request.getParameter("tauxAlcool"));
		request.getSession().setAttribute("catAjout", request.getParameter("categorie"));
		try {
			mrb.ChercherBrasseurs();
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}	
		try {
			mlc.ChercherCategorie();
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
		request.setAttribute("modBrasseur", mrb);
		request.setAttribute("modListCat", mlc);

		ControleurAdmin.vue = "/WEB-INF/vues/gabarit-vues.jsp";
		ControleurAdmin.vueContenu = "/WEB-INF/vues/admin/ajout-biere.jsp";
		ControleurAdmin.vueSousTitre = "Ajout d'une bière";
	}
	
	private static void postAjoutBiereAdmin(HttpServletRequest request)throws ServletException, IOException
	{
		String nomBiere = "";
		String nomBrasseur = "";
		String description = "";
		String tauxAlcool = "";
		String categorie = "";
		
		
		ModelBrasseurDetail mrb = new ModelBrasseurDetail();
		ModeleListCategorie mlc = new ModeleListCategorie();
		ModelAjoutBiere mab = new ModelAjoutBiere();

		FileItem itemphoto = null;
						   
		  try{

		   List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(new ServletRequestContext(request));
		   for(FileItem item:items){

			if (!item.isFormField())
			{
			
				itemphoto = item;

			}
			else
			{
				 switch (item.getFieldName()) {
		            case "nomBiere":  nomBiere = item.getString();
		                     break;
		            case "nomBrasseur": nomBrasseur = item.getString();
		                     break;
		            case "description": description = item.getString();
		                     break;
		            case "tauxAlcool":  tauxAlcool = item.getString();
		                     break;
		            case "categorie":  categorie = item.getString();
		                     break;

		            default:
		                     break;
		        }

			}
		   }
		  }
		  catch(FileUploadException e){
		    
			  request.setAttribute("erreurphoto", "Erreur: le system ne peut pas enregistrer votre image");

		  }
		  catch(Exception ex){
		    
			  request.setAttribute("erreurphoto", "Erreur: le system ne peut pas enregistrer votre image");

		  }
			  

			try {
				mab.ajouterBiere(nomBrasseur, nomBiere, categorie, tauxAlcool, description, itemphoto);
			} catch (NamingException | SQLException e) {
				throw new ServletException(e);
			}	

			request.setAttribute("modAjoutBiere", mab);
		if(mab.getMsgErreur() != null)
		{
			request.getSession().setAttribute("nomAjout", nomBiere);
			request.getSession().setAttribute("brasseurAjout", nomBrasseur);
			request.getSession().setAttribute("descriptionAjout", description);
			request.getSession().setAttribute("tauxAlcoolAjout", tauxAlcool);
			request.getSession().setAttribute("catAjout", categorie);
		}
		else
		{
			request.setAttribute("BiereAjoute", "Félicitation :D !!! Votre bière a été ajouté !");
			request.getSession().removeAttribute("nomAjout");
			request.getSession().removeAttribute("brasseurAjout");
			request.getSession().removeAttribute("descriptionAjout");
			request.getSession().removeAttribute("tauxAlcoolAjout");
			request.getSession().removeAttribute("catAjout");
		}


		
		try {
			mrb.ChercherBrasseurs();
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}	
		try {
			mlc.ChercherCategorie();
		} catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
		request.setAttribute("modBrasseur", mrb);
		request.setAttribute("modListCat", mlc);
		
		ControleurAdmin.vue = "/WEB-INF/vues/gabarit-vues.jsp";
		ControleurAdmin.vueContenu = "/WEB-INF/vues/admin/ajout-biere.jsp";
		ControleurAdmin.vueSousTitre = "Ajout d'une bière";
	}
	

}
