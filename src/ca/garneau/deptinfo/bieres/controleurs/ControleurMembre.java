package ca.garneau.deptinfo.bieres.controleurs;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ca.garneau.deptinfo.bieres.beans.ConnexionBean;
import ca.garneau.deptinfo.bieres.classes.ConnexionMode;
import ca.garneau.deptinfo.bieres.modeles.ModelAccueilMembre;
import ca.garneau.deptinfo.bieres.modeles.ModelCritique;

import javax.servlet.annotation.MultipartConfig;

@MultipartConfig
public class ControleurMembre extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	// Attributs
	// =========
	/**
	 * URI sans le context path.
	 */
	protected static String uri;
	
	/**
	 * Vue à afficher (chemin du fichier sur le serveur).
	 */
	protected static String vue;
	
	/**
	 * Fragment de la vue (chemin du fichier sur le serveur) à charger
	 * dans la zone de contenu si la vue est créée à partir du gabarit.
	 */
	protected static String vueContenu;
	
	/**
	 * Sous-titre de la vue si la vue est créée à partir du gabarit.
	 */
	protected static String vueSousTitre;
	
	/**
	 * Permet d'effectuer les traitements avant de gérer la ressource demandée.
	 * @param request La requête HTTP.
	 * @param response La réponse HTTP.
	 * @return true si les opérations se sont bien déroulées; false, autrement.
	 * @throws IOException 
	 */
	protected static boolean preTraitement(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// Récupération de l'URI sans le context path.
		ControleurMembre.uri = request.getRequestURI().substring(request.getContextPath().length());
		ControleurMembre.vue = null;
		ControleurMembre.vueContenu = null;
		ControleurMembre.vueSousTitre = null;
		
		// Expiration de la cache pour les pages de cette section.		
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");  // HTTP 1.1.
		response.setHeader("Pragma", "no-cache");  // HTTP 1.0.
		response.setDateHeader("Expires", 0);  // Proxies.

		ConnexionBean unUser = (ConnexionBean)request.getSession().getAttribute("util"); 
		
		// Contrôle d'accès à la section pour les employés.
		if (unUser == null || unUser.getModeConn() != ConnexionMode.MEMBRE) {
			// Non connecté en tant qu'admin; on retourne un code d'erreur
			// HTTP 401 qui sera intercepté par la page d'erreur "erreur-401.jsp".
			response.sendError(401);
			return false;
		}
		else
			return true;
	}

	/**
	 * Permet de gérer les ressources GET suivantes :
	 * 		"/admin/" ou "/admin"		:	Accueil pour le personnel
	 *		"/admin/critiques"			:	Liste des critiques
	 *		"/admin/ajout-biere"		:	Ajout d'une bière
	 * 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if (preTraitement(request, response)) {

			if (ControleurMembre.uri.equals("/membre/") || ControleurMembre.uri.equals("/membre")) {
				pageAccueilMembreGet(request);
			} else if (ControleurMembre.uri.equals("/membre/ajout-commentaire")) {
				pageAjoutCritiqueGet(request);
			}else {
				response.sendError(404);

			}
			
		}

		postTraitement(request, response);

	}
	
	/**
	 * Permet d'effectuer les traitements suite à la gestion de la ressource demandée.
	 * @param request La requête HTTP.
	 * @param response La réponse HTTP.
	 */
	protected void postTraitement(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (ControleurMembre.vue != null) {
			if (ControleurMembre.vueContenu != null || ControleurMembre.vueSousTitre != null) {
				request.setAttribute("vueContenu", ControleurMembre.vueContenu);
				request.setAttribute("vueSousTitre", ControleurMembre.vueSousTitre);
			}
			request.getRequestDispatcher(ControleurMembre.vue).forward(request, response);
		}		
	}	

	private static void pageAccueilMembreGet(HttpServletRequest request)throws ServletException, IOException 
	{
		ModelAccueilMembre mam = new ModelAccueilMembre();
		try{
			ConnexionBean unUser = (ConnexionBean)request.getSession().getAttribute("util");
			mam.rechercheCritique(Integer.toString(unUser.getNoUtil()));
		}
		catch (NamingException | SQLException e) {
			throw new ServletException(e);
		}
		request.setAttribute("modAccueilMembre", mam);
		ControleurMembre.vue = "/WEB-INF/vues/gabarit-vues.jsp";
		ControleurMembre.vueContenu = "/WEB-INF/vues/membre/accueil-membre.jsp";
		ConnexionBean unUser = (ConnexionBean)request.getSession().getAttribute("util"); 
		ControleurMembre.vueSousTitre = "Page personnelle de " + unUser.getNomUtil();
	}
	
	private static void pageAjoutCritiqueGet(HttpServletRequest request)throws ServletException, IOException 
	{
		ModelCritique mc = new ModelCritique();

		if (request.getParameter("ajout-critique") != null)
		{
			try {
				ConnexionBean unUser = (ConnexionBean)request.getSession().getAttribute("util"); 
				String strIdUser = Integer.toString(unUser.getNoUtil());
				mc.ajouterCritique(
						strIdUser,
						request.getParameter("noBiere"),
						request.getParameter("arome"),
						request.getParameter("apparence"),
						request.getParameter("gout"),
						request.getParameter("style"),
						request.getParameter("general")
						);
			} catch (NamingException | SQLException e) {
				throw new ServletException(e);
			}
			
			if(mc.getMsgErreur() != null)
			{
				request.getSession().setAttribute("AromeAjout", request.getParameter("arome"));
				request.getSession().setAttribute("ApparenceAjout", request.getParameter("apparence"));
				request.getSession().setAttribute("GoutAjout", request.getParameter("gout"));
				request.getSession().setAttribute("StyleAjout", request.getParameter("style"));
				request.getSession().setAttribute("GeneralAjout", request.getParameter("general"));
			}
			else
			{
				request.setAttribute("CritiqueAjoute", "Félicitation :D !!! Votre critique a été ajouté !");
				request.getSession().removeAttribute("AromeAjout");
				request.getSession().removeAttribute("ApparenceAjout");
				request.getSession().removeAttribute("GoutAjout");
				request.getSession().removeAttribute("StyleAjout");
				request.getSession().removeAttribute("GeneralAjout");
			}
			
		}
		request.setAttribute("modAjoutCommentaire", mc);
		ControleurMembre.vue = "/WEB-INF/vues/gabarit-vues.jsp";
		ControleurMembre.vueContenu = "/WEB-INF/vues/membre/ajout-commentaire.jsp";
		ControleurMembre.vueSousTitre = "Ajout d'un commentaire";
	}

}
