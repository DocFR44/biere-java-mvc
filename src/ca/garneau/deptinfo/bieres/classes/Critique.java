package ca.garneau.deptinfo.bieres.classes;

public class Critique {
	private String date_creation;
	private String nomBiere;
	private int arome;
	private int apparence;
	private int gout;
	private int style_bouteille;
	private int general;
	
	public Critique(String date_creation, String nomBiere, int arome, int apparence, int gout, int style_bouteille, int general) {
		this.nomBiere = nomBiere;
		this.arome = arome;
		this.apparence = apparence;
		this.gout = gout;
		this.style_bouteille = style_bouteille;
		this.general = general;
		this.date_creation = date_creation;
	}
	public String getDate_creation() {
		return date_creation;
	}
	public void setDate_creation(String date_creation) {
		this.date_creation = date_creation;
	}
	public String getNomBiere() {
		return nomBiere;
	}
	public void setNomBiere(String nomBiere) {
		this.nomBiere = nomBiere;
	}
	public int getArome() {
		return arome;
	}
	public void setArome(int arome) {
		this.arome = arome;
	}
	public int getApparence() {
		return apparence;
	}
	public void setApparence(int apparence) {
		this.apparence = apparence;
	}
	public int getGout() {
		return gout;
	}
	public void setGout(int gout) {
		this.gout = gout;
	}
	public int getStyle_bouteille() {
		return style_bouteille;
	}
	public void setStyle_bouteille(int style_bouteille) {
		this.style_bouteille = style_bouteille;
	}
	public int getGeneral() {
		return general;
	}
	public void setGeneral(int general) {
		this.general = general;
	}


}
