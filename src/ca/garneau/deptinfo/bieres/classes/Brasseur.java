package ca.garneau.deptinfo.bieres.classes;

public class Brasseur {
	
	private String id;
	
	private String nom;
	
	private String adresse;
	
	private String ville;
	
	private String etat_prov;
	
	private String codePostal;
	
	private String pays;
	
	private String telephone;
	
	private String siteWeb;
	
	private String description;
	
	private String img;

	public Brasseur(String id, String nom, String adresse, String ville, String etat_prov, String codePostal, String pays,
			String telephone, String siteWeb, String description, String img) {
		super();
		this.id = id;
		this.nom = nom;
		this.adresse = adresse;
		this.ville = ville;
		this.etat_prov = etat_prov;
		this.codePostal = codePostal;
		this.pays = pays;
		this.telephone = telephone;
		this.siteWeb = siteWeb;
		this.description = description;
		this.img = img;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getEtat_prov() {
		return etat_prov;
	}

	public void setEtat_prov(String etat_prov) {
		this.etat_prov = etat_prov;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getSiteWeb() {
		return siteWeb;
	}

	public void setSiteWeb(String siteWeb) {
		this.siteWeb = siteWeb;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}
	
	

}
