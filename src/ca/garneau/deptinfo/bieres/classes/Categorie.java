package ca.garneau.deptinfo.bieres.classes;

public class Categorie {
	private String id;
	private String nom;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Categorie(String id, String nom) {
		super();
		this.id = id;
		this.nom = nom;
	}
	

}
